<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
			header('Access-Control-Allow-Credentials:true');
			header('Access-Control-Request-Headers: *');
			header('Content-Type: plain/text; charset=UTF-8');
			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Request-Method:*');
			
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c")
		{
			if($_POST['LoginForm']['username'] == null){
				echo "No has introducido el nombre de usuario";
				exit(0);
			}
			$usuario = Jugador::model()->find('username ="'.$_POST['LoginForm']['username'].'"');
			
			if($usuario == null){
				echo "Hay un error en el usuario o contraseña";
				exit(0);
			}
			
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			
			if($model->login()) {
				//$this->redirect(Yii::app()->user->returnUrl);
				$session=new CHttpSession;
				$session->open();
				$jugadorNom = Jugador::model()->findByPk($session['id']);
				$array = array(
					'msg'=>"ok",
					'id'=>$session['id'],
					'username'=>$jugadorNom->username,
					'puntuacio'=>$jugadorNom->puntuacio
				);
				echo json_encode($array);
				exit(0);
			}
			else {
				echo "Hay un error en el usuario o contraseña";
				exit(0);
			}
				
		}
		else {
			echo "Introduce los datos";
			exit(0);
		}
		// display the login form
		//$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
