<?php

class JugadorController extends Controller
{
	
	public $layout = false;
		
	public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }


	public function actionRegistre(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');

		$model = new Jugador;
		
		

		if(isset($_POST['Registro']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
			$search = Jugador::model()->find('username = "'.$_POST['Registro']['username'].'"');
			
			if($search!=null){
				echo "El usuari ja existeix";
				exit(0);
			}
			
			$model->username = $_POST['Registro']['username'];
			$model->email = $_POST['Registro']['email'];
			$model->password = $model->hashPassword($_POST['Registro']['password']);
			
			if($model->save()){
				echo "S'ha creat correctament el usuari";
				exit(0);
			}
			else {
				echo "Revisa que les dades siguin correctes";
				exit(0);
			}
			
		}
	}
	
	public function actionHistorialonline(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
		
		
		
		if(isset($_POST['Historial']['id']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c") {
			
				$puntuacio = Jugador::model()->findByPk($_POST['Historial']['id'])->puntuacio;
				
				if($puntuacio > 999){
					$puntuacio = (string)$puntuacio;
					$array['nivell'] = $puntuacio[0]."".$puntuacio[1];
					$array['restant'] = $puntuacio[2]."".$puntuacio[3];
				}
				else {
					$puntuacio = (string)$puntuacio;
					if(isset($puntuacio[2])){
						$array['nivell'] = $puntuacio[0];
						$array['restant'] = $puntuacio[1]."".$puntuacio[2];
					}
					else {
						$array['nivell'] = 0;
						if($puntuacio == "0"){
							$array['restant'] = 0;
						}
						else{
							$array['restant'] = $puntuacio[0]."".$puntuacio[1];
						}
						
					}
					
				}
				$model = PartidaOnline::model()->findAll('id_jugador1 ='.$_POST['Historial']['id'].' OR id_jugador2 = '.$_POST['Historial']['id']);
				if($model == null){
					$array['guanyades'] = 0;
					$array['perdudes'] = 0;
					$array['jugades'] = 0;
					echo json_encode($array);
					exit(0);
				}
				else {
					$array['jugades'] = count($model);
				}
		
				$array['perdudes'] = 0;
				$array['guanyades'] = 0;
				
				foreach($model as $key => $value){
					if($value->id_guanyador!=null){
						$array['historial'][] = array(
						'jugador1'=> Jugador::model()->findByPk($value->id_jugador1)->username,
						'jugador2'=> Jugador::model()->findByPk($value->id_jugador2)->username,
						'nomGuanyador'=> Jugador::model()->findByPk($value->id_guanyador)->username
						);
						
						if($value->id_guanyador == $_POST['Historial']['id']){
							$array['guanyades'] = $array['guanyades'] + 1;
						}
						else{
							$array['perdudes'] = $array['perdudes'] +1;
						}
					}
				}
				
				echo json_encode($array);
		}
	}
	
	
	public function actionGetAmics(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
		$model = Amic::model()->findAll('id_jugador1 ='.$_POST['Amic']['id'].' OR id_jugador2 = '.$_POST['Amic']['id']);
		//$model = Amic::model()->findAll('id_jugador1 = 1 OR id_jugador2 = 1');
		if($model!= null && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
				foreach($model as $key => $value){
					if($value->id_jugador1 != $_POST['Amic']['id']){
						if($value->actiu == 1){
							$array['amics'][] = array(
								'id'=> $value->id_jugador1,
								'nom'=> Jugador::model()->findByPk($value->id_jugador1)->username,
								'idConsulta'=>$value->id
							);
						}
						else {
							if($value->actiu == 0){
								$array['pendent'][] = array(
									'id'=> $value->id_jugador1,
									'nom'=> Jugador::model()->findByPk($value->id_jugador1)->username,
									'idConsulta'=>$value->id
								);
							}
							else{
								$array['elimitats'][] = array(
									'id'=> $value->id_jugador1,
									'nom'=> Jugador::model()->findByPk($value->id_jugador1)->username,
									'idConsulta'=>$value->id
								);
							}
						}
					}
					else if($value->id_jugador2 != $_POST['Amic']['id']){
						if($value->actiu == 1){
							$array['amics'][] = array(
								'id'=> $value->id_jugador2,
								'nom'=> Jugador::model()->findByPk($value->id_jugador2)->username,
								'idConsulta'=>$value->id
							);
						}
						else {
							if($value->actiu == 0){
								$array['esperant'][] = array(
									'id'=> $value->id_jugador2,
									'nom'=> Jugador::model()->findByPk($value->id_jugador2)->username,
									'idConsulta'=>$value->id
								);
							}
							else{
								$array['elimitats'][] = array(
									'id'=> $value->id_jugador2,
									'nom'=> Jugador::model()->findByPk($value->id_jugador2)->username,
									'idConsulta'=>$value->id
								);
							}
						}

					}
				}
				echo json_encode($array);
		}

	}
	
	public function actionsendNoti(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
		
		$entra = true;
		
		if(isset($_POST['NotiAmic']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
			$user = Jugador::model()->find('username ="'.$_POST['NotiAmic']['username'].'"');
			if($user != null){
				$idUser = $user->id;
				if($idUser!=$_POST['NotiAmic']['id']){
					$check = Amic::model()->findAll('id_jugador1='.$_POST['NotiAmic']['id'].' OR id_jugador2='.$_POST['NotiAmic']['id']);
					if($check!=null){
						foreach($check as $key => $value){
							if(($value->id_jugador1==$idUser || $value->id_jugador2==$idUser) && $value->actiu != 2){
								echo "Ya tienes agregado este usuario";
								$entra = false;
							}
						}
						if($entra==true){
							$model = new Amic;
							$model->id_jugador1 = $_POST['NotiAmic']['id'];
							$model->id_jugador2 = $idUser;
							$model->save();
						}
					}
					else {
						$model = new Amic;
						$model->id_jugador1 = $_POST['NotiAmic']['id'];
						$model->id_jugador2 = $idUser;
						$model->save();
					}
				}
				else {
					echo "No puedes agregarte a ti mismo";
					exit(0);
				}	
				
			}
			else {
				echo "No existe el usuario";
				exit(0);
			}	
		}
	}
	
	public function actionAcceptNotiAmic(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
		
		if(isset($_POST['NotiAmic']['id']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
			$model = Amic::model()->findByPk($_POST['NotiAmic']['id']);
			if($model != null){
				$model->actiu = 1;
				$model->save();
			}
		}
		
		
	}
	
	public function actionCreatematch(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');

		if(isset($_POST['CreateMatch']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
			$match = new PartidaOnline;
			$match->attributes = $_POST['CreateMatch'];
			if($match->save()){
				echo "Se ha creado correctamente la partida";
				exit(0);
			}
			else {
				echo "No se ha podido crear la partida";
				exit(0);
			}
		}
	}
	
	
	public function actionGetmatches(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
		$entra = false;
		if(isset($_POST['Matches']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
			$model = PartidaOnline::model()->findAll('id_jugador1 ='.$_POST['Matches']['id'].' OR id_jugador2 = '.$_POST['Matches']['id']);
			if($model!=null){
				foreach($model as $key => $value){
					if($value->id_guanyador==null){
						if($value->id_jugador1 == $_POST['Matches']['id']){
							if($value->temps_jugador1 == null){
								$array['PendentJugar'][]= array(
									'id'=>$value->id,
									'distancia'=>$value->distancia,
									'nom'=>Jugador::model()->findByPk($value->id_jugador2)->username
								);
							}
							else if($value->temps_jugador2 == null){
								$array['EsperaJugar'][]= array(
									'id'=>$value->id,
									'distancia'=>$value->distancia,
									'nom'=>Jugador::model()->findByPk($value->id_jugador2)->username
								);
							}
							$entra = true;
						}
						else {
							if($value->temps_jugador2 == null){
								$array['PendentJugar'][]= array(
									'id'=>$value->id,
									'distancia'=>$value->distancia,
									'nom'=>Jugador::model()->findByPk($value->id_jugador1)->username
								);
							}
							else if($value->temps_jugador1 == null){
								$array['EsperaJugar'][]= array(
									'id'=>$value->id,
									'distancia'=>$value->distancia,
									'nom'=>Jugador::model()->findByPk($value->id_jugador1)->username
								);
							}
							$entra = true;
						}
					}
				}
				if($entra == true){
					echo json_encode($array);
				}				
			}	
		}	
	}
	
	
	public function actionGetPartida(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
		
		if(isset($_POST['Partida']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
			$model = PartidaOnline::model()->findByPk($_POST['Partida']['id']);
			if($model!=null){
				if($model->id_jugador1 == $_POST['Partida']['usuari'] || $model->id_jugador2 == $_POST['Partida']['usuari'] ){
					$array['distancia'] = $model->distancia;
				}
			}
			echo json_encode($array);
		}
		
		
		
	}
	
	public function actionObtepartida(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
		
		if(isset($_POST['Partida']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
			$model = PartidaOnline::model()->findByPk($_POST['Partida']['id']);
			if($model!=null){
				if($model->id_jugador1 == $_POST['Partida']['usuari'] || $model->id_jugador2 == $_POST['Partida']['usuari'] ){
					$array['distancia'] = $model->distancia;
				}
			}
		}
		
		echo json_encode($array);	
	}
	
	public function actionsavePartida(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
		
		if(isset($_POST['Partida']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
			$model = PartidaOnline::model()->findByPk($_POST['Partida']['id']);
			if($model!=null){
				if($model->id_jugador1 == $_POST['Partida']['usuari']){
					$model->temps_jugador1 = $_POST['Partida']['temps'];
					if($model->temps_jugador2 != null){
						if($model->temps_jugador1 > $model->temps_jugador2){
							$model->id_guanyador = $model->id_jugador2;
						}
						else if($model->temps_jugador1 < $model->temps_jugador2){
							$model->id_guanyador = $model->id_jugador1;
						}
						else {
							$random = rand(1,2);
							if($random == 1){
								$model->id_guanyador = $model->id_jugador1;
							}
							else {
								$model->id_guanyador = $model->id_jugador2;
							}
						}	
					}
					$model->save();
				}
				else if($model->id_jugador2 == $_POST['Partida']['usuari']){
					$model->temps_jugador2 = $_POST['Partida']['temps'];
					if($model->temps_jugador1 != null){
						if($model->temps_jugador1 > $model->temps_jugador2){
							$model->id_guanyador = $model->id_jugador2;
						}
						else if($model->temps_jugador1 < $model->temps_jugador2){
							$model->id_guanyador = $model->id_jugador1;
						}
						else {
							$random = rand(1, 2);
							if($random == 1){
								$model->id_guanyador = $model->id_jugador1;	
							}
							else {
								$model->id_guanyador = $model->id_jugador2;
							}
						}	
					}
					$model->save();
				}
			}
		}
	}



	public function actionsavePartidaOff(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
		
		$model = new PartidaOffline;
		
		if(isset($_POST['PartidaOff']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
			$user = Jugador::model()->findByPk($_POST['PartidaOff']['id_jugador']);
			$user->puntuacio = $user->puntuacio + 10;
			$user->save();
			$model->attributes = $_POST['PartidaOff'];	
			if(!$model->save()){
				print_r($model->getErrors());
			}
		}
	}
	
	
	
	public function actiontotalPartidesOnline(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
		
		if(isset($_POST['Total']['usuari'])){
			$model = PartidaOnline::model()->findAll('id_jugador2 = '.$_POST['Total']['usuari'].' OR id_jugador1 ='.$_POST['Total']['usuari']);
			$cont= 0;
			foreach($model as $key => $value){
				if($value->id_jugador1 == $_POST['Total']['usuari'] && $value->temps_jugador1 == null){
					$cont++;
				}
				else if($value->id_jugador2 == $_POST['Total']['usuari'] && $value->temps_jugador2 == null){
					$cont++;
				}
			}
			$array['total'] = $cont;
			echo json_encode($array);
		}
	}
	
	public function actionEliminaAmic(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
				
		if(isset($_POST['EliminaAmic']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
			$user = Jugador::model()->findByPk($_POST['EliminaAmic']['userId']);
			if($user != null){
				$check = Amic::model()->findByPk($_POST['EliminaAmic']['id']);
				if($check!=null){
					$check->actiu = 2;
					$check->save();
					echo "Has eliminado correctamente el usuario";
				}
				else {
					echo "No puedes eliminar el usuario";
				}
			}
			else {
				echo "No puedes eliminar el usuario";
			}	
		}
	}
	
	public function actionBuscaPartida(){
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers');
		header('Access-Control-Allow-Credentials:true');
		header('Access-Control-Request-Headers: *');
		header('Content-Type: plain/text; charset=UTF-8');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Request-Method:*');
				
		if(isset($_POST['BuscaPartida']) && $_POST['token']=="657a6b24e9392ea41f5b0eca995ba67c"){
			$model = PartidaOnline::model()->find('id_jugador2 = null');
			if($model==null){
				echo "No hay partidas existentes, crea una nueva partida o invita a un amigo!";
			}
			else{
				$entra = true;
				foreach($model as $key => $value){
					if($value->distancia<=$_POST['BuscaPartida']['distancia'] && $entra == true){
						$id = $value->id;
						$entra = false;
					}
				}
				if($id!=null){
					$model = PartidaOnline::model()->findByPk($id);
					$model->id_jugador2 = $_POST['BuscaPartida']['id'];
					$model->save();
					echo "¡Ha encontrado una nueva partida!";
				}
				else {
					echo "No hay partidas existentes con tu condición";
				}
			}
		}
	}
	
	
	
	
	

		
}
