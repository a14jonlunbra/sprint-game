<?php

Yii::import('application.models._base.BaseJugador');

class Jugador extends BaseJugador
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public static function validatePassword($password, $password2)
    {		
		if (CPasswordHelper::verifyPassword($password, $password2)){
			return true;
		}
		else{
			return false;
		}
    }
 
    public static function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }
	
	
	
}
