<?php

/**
 * This is the model base class for the table "Amics".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Amic".
 *
 * Columns in table "Amics" available as properties of the model,
 * followed by relations of table "Amics" available as properties of the model.
 *
 * @property integer $id
 * @property integer $id_jugador1
 * @property integer $id_jugador2
 * @property integer $actiu
 *
 * @property Jugador $idJugador1
 * @property Jugador $idJugador2
 */
abstract class BaseAmic extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'Amics';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Amic|Amics', $n);
	}

	public static function representingColumn() {
		return 'id';
	}

	public function rules() {
		return array(
			array('id_jugador1, id_jugador2', 'required'),
			array('id_jugador1, id_jugador2, actiu', 'numerical', 'integerOnly'=>true),
			array('actiu', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, id_jugador1, id_jugador2, actiu', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'idJugador1' => array(self::BELONGS_TO, 'Jugador', 'id_jugador1'),
			'idJugador2' => array(self::BELONGS_TO, 'Jugador', 'id_jugador2'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'id_jugador1' => null,
			'id_jugador2' => null,
			'actiu' => Yii::t('app', 'Actiu'),
			'idJugador1' => null,
			'idJugador2' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('id_jugador1', $this->id_jugador1);
		$criteria->compare('id_jugador2', $this->id_jugador2);
		$criteria->compare('actiu', $this->actiu);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}