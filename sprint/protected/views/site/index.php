    
        <div class="barIconFixed">
            <img src="<?php echo Yii::app()->baseUrl; ?>/imagenes/icon.png" class="imgfija bounceIn animated" data-container="body" data-toggle="popover" data-placement="top" data-html="true" data-content='<img src="imagenes/appstore.png" class="miniicon bounceIn animated"><img src="imagenes/android.png" class="miniicon bounceIn animated">'>

        </div>
        <!--FOOTER-->
        <header>
                <section class="row header">
                    <article class="hidden-xs col-sm-5 col-md-5 col-sm-offset-1">
                       <img src="<?php echo Yii::app()->baseUrl; ?>/imagenes/logo-spring.png" class="logosprint bounceIn animated">
                        <div class="logos">
                            <img src="<?php echo Yii::app()->baseUrl; ?>/imagenes/appstore.png" class="appstore bounceIn animated">
                            <img src="<?php echo Yii::app()->baseUrl; ?>/imagenes/android.png" class="android bounceIn animated">
                        </div>
                    </article>
                    <article class="col-xs-12 col-sm-4 col-sm-offset-2 col-md-4 col-md-offset-2">
                        <img src="<?php echo Yii::app()->baseUrl; ?>/imagenes/iphone.png" alt="fotografia iphone 6s" class="imagen-background bounceIn animated" id="iphone">
                    </article>
                </section>
            
        </header>
        <!--MAIN-->
        <main>
            <section id="acerca" class="row height-seccion-main background-sections">
                <article class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                    <div class="media infoApp wow animated fadeInLeft padding_main">
                        <a class="media-left med" href="#">
                            <p class="circulo"><span class="fa fa-cogs" aria-hidden="true"></span></p>
                        </a>
                        <div class="media-body">
                             Creus que ets el més ràpid fent scroll? Juga a Sprint Game i demostra que ho ets superant tots els nivells i juga amb gent de tot el món!
                        </div>
                    </div>    
                    <div class="media infoApp wow animated fadeInLeft">
                        <a class="media-left med" href="#">
                            <p class="circulo"><span class="fa fa-users" aria-hidden="true"></span></p>
                        </a>
                        <div class="media-body">
                             Afegeix a un amic, i juga contra ell en una partida personalitzada! Ara podràs demostrar qui és el més ràpid dels teus amics.
                        </div>
                    </div>    
                    <div class="media infoApp wow animated fadeInLeft">
                        <a class="media-left med" href="#">
                            <p class="circulo"><span class="fa fa-gamepad" aria-hidden="true"></span></p>
                        </a>
                        <div class="media-body">
                             Busca una partida, juga contra qualsevol amic de tot el món, i si et guanya, reclama-li una revenja!
                        </div>
                    </div>           
                </article>
                <article class="col-sm-6 col-md-4 col-md-offset-2"></article>
            </section>
            <section id="multijugador" class="row height-seccion-main background-sections">
                <div class="wow animated fadeInLeft ">
                    <article class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                        <h2>Comença a divertir-te!<span> Afegeix a un amic</span></h2>
                        <img alt="Juga amb els teus amics! Sprint Game" src="<?php echo Yii::app()->baseUrl; ?>/imagenes/friends.png" width="300"/>
                    </article>
                    <article class=" col-sm-6 col-md-4 col-md-offset-2">
	                    
                    </article>
                </div>
            </section>

            <section id="amics" class="row height-seccion-main background-sections">
                <article class=" col-sm-6 col-md-5 col-md-offset-1">
                    <div class="wow animated fadeInLeft padding_main1">   
                        <h2>Segueix-nos a travès de</h2>
                        <div class="icons_amics row">
                        <div class="col-xs-4 col-md-4 col-lg-3">
                            <a href="https://www.instagram.com/" class="linksmain"><span class="fa fa-instagram" aria-hidden="true"></span></a>
                            </div>
                            <div class="col-xs-4 col-md-4 col-lg-3">
                            <a href="https://www.facebook.com/" class="linksmain"><span class="fa fa-facebook" aria-hidden="true"></span></a>
                            </div>
                            <div class="col-xs-4 col-md-4 col-lg-3">
                            <a href="https://twitter.com/" class="linksmain"><span class="fa fa-twitter" aria-hidden="true"></span></a>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="col-sm-6 col-md-4 col-md-offset-2">
                    <div class="wow animated fadeInRight padding_main">
                        <h2>Projecte CFGS</h2>
                        <a href="http://www.institutausiasmarch.cat/pam/"><img src="<?php echo Yii::app()->baseUrl; ?>/imagenes/iam.png" alt="logo centre ausias march" class="img-responsive"></a>
                    </div>
                </article>
            </section>

            <section id="developers" class="row height-seccion-main1 background-sections">
                <article class="col-sm-6 col-md-6">
                    <div class="developers wow animated fadeInLeft">
                        <img alt="Carles Grau Vilanova" class="img-thumbnail imgDeveloper" src="<?php echo Yii::app()->baseUrl; ?>/imagenes/carles.jpg" alt="fotografia carles">
                        <p class="nameDevelop">Carles Grau Vilanova<span> Web Developer</span></p>
                        <p class="descripDevelop">Ambiciós amb ganes d'aprendre i invertir per aconseguir nous objectius a la vida.</p>
                        <div class="ifooter">
                            <a href="https://es.linkedin.com/in/carles-grau-vilanova-abb913b5"><span class="fa fa-linkedin-square" aria-hidden="true"></span></a>
                            <a href="#"><span class="fa fa-instagram" aria-hidden="true"></span></a>
                            <a href="https://bitbucket.org/carlesgrauvila/"><span class="fa fa-bitbucket-square" aria-hidden="true"></span></a>
                        </div>
                    </div>
                </article>
                <article class="col-sm-6 col-md-6">
                    <div class="developers wow animated fadeInRight">
                        <img alt="Jonatan Luna Branchart" class="img-thumbnail imgDeveloper" src="<?php echo Yii::app()->baseUrl; ?>/imagenes/jonatan.jpg" alt="fotografia jonatan">
                        <p class="nameDevelop">Jonatan Luna Branchat<span> Web Developer</span></p>
                        <p class="descripDevelop">Invertir hores diàries per aprendre nous llenguatges, és una de les meves qualitats.</p>
                        <div class="ifooter">
                            <a href="https://es.linkedin.com/in/jonatan-luna-branchat-b1a74811a"><span class="fa fa-linkedin-square" aria-hidden="true"></span></a>
                             <a href="#"><span class="fa fa-instagram" aria-hidden="true"></span></a>
                             <a href="https://bitbucket.org/a14jonlunbra/"><span class="fa fa-bitbucket-square" aria-hidden="true"></span></a>
                        </div>
                    </div>
                </article>
            </section>
        </main>