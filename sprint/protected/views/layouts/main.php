<!DOCTYPE html>
<html lang="es">
<head>
	<title>Sprint - Game of Scroll</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />

    <!-- link para la fuente "Montserrat" -->
    <link href='//fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <!-- libreria de efectos animados animate.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/animate.css">
	<!-- Cargar el css de bootrsap-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap/dist/css/bootstrap.min.css">
    <!-- Carga iconos css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<!-- Carga de metas-->
    <!-- Libreria WOW para cargar el css en la posicion del scroll -->
    <script src="<?php echo Yii::app()->baseUrl; ?>/css/wow.min.js"></script>
    <!-- Libreria WOW para cargar el css en la posicion del scroll -->

	<!-- Carga de fichero index.css y index.js-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/index.css">
    <!-- Inicializamos el objeto wow para crear las animaciones cuando hacemos scroll -->
    <script type="text/javascript">
        new WOW().init();
    </script>
</head>
<body>
	<div class="container-fluid">

	<?php echo $content; ?>

	        <footer>
	            <section class="row footer">
	                  <span>© Sprint Game</span>  
	            </section>
        	</footer>
    </div>

    <script src="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap/js/jquery.js"></script>
    <script src="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap/dist/js/bootstrap.min.js"></script>
    <script>
       
        var top = 72;
        $(function(){
            if($("body").width() > 767){
                $(document).scroll(function(){
                 var pos = $(this).scrollTop();
                 var div_header = $(".header").height();
                 var div_main1 = 868;
                 //posicion respecto al top de la imagen
                 if(pos+200 <= div_header){
                    $('#iphone').attr('src','imagenes/iphone.png');
                 }
                    else if((pos+200 > div_header) && (pos+200 < div_main1)){
                       $('#iphone').attr('src','imagenes/iphone2.png');
                    }
                       else if(pos+200 > div_main1){
                            $('#iphone').attr('src','imagenes/iphone3.png');
                            if(pos>div_main1){
                               $("#iphone").css({
                                    "position":"absolute",
                                    'top': 930+'px'
                                 }); 
                            }
                            else{
                                $("#iphone").css({'position':'fixed','top':'72px'});
                            }
                            
                        }
                 });
            }
            else {
              $(document).scroll(function(){
                $('.imgfija').popover('hide');
              });  
            }
        });
    </script>
    <script>
         $(document).ready(function(){
             $('.imgfija').popover();
         });
    </script>
</body>
</html>