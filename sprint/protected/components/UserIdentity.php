<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{	
		$username=$this->username;
		$jugador=Jugador::model()->find('username="'.$username.'"');
		if($jugador===null){
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}
		if(!$jugador->validatePassword($this->password, $jugador->password)){
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		}
		else {
			$this->username=$jugador->username;
			$session=new CHttpSession;
			$session->open();
			$session['id']=$jugador->id;
			$this->errorCode=self::ERROR_NONE;
		}
		
		return $this->errorCode==self::ERROR_NONE;
	}

	
	
}
