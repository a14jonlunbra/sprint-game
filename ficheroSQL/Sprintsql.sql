SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `SprintGame` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `SprintGame` ;

-- -----------------------------------------------------
-- Table `SprintGame`.`Jugador`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `SprintGame`.`Jugador` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `nom` VARCHAR(45) NULL ,
  `cognom` VARCHAR(45) NULL ,
  `username` VARCHAR(45) NOT NULL ,
  `email` VARCHAR(45) NOT NULL ,
  `password` VARCHAR(45) NOT NULL ,
  `puntuacio` INT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SprintGame`.`Amics`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `SprintGame`.`Amics` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `id_jugador1` INT NOT NULL ,
  `id_jugador2` INT NOT NULL ,
  `actiu` TINYINT(1) NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Amics_Jugador1` (`id_jugador1` ASC) ,
  INDEX `fk_Amics_Jugador2` (`id_jugador2` ASC) ,
  CONSTRAINT `fk_Amics_Jugador1`
    FOREIGN KEY (`id_jugador1` )
    REFERENCES `SprintGame`.`Jugador` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Amics_Jugador2`
    FOREIGN KEY (`id_jugador2` )
    REFERENCES `SprintGame`.`Jugador` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SprintGame`.`Partida_Offline`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `SprintGame`.`Partida_Offline` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `puntuacio_partida` INT NOT NULL ,
  `guanyada` TINYINT(1) NULL ,
  `temps_realitzat` TIME NULL ,
  `temps` TIME NOT NULL ,
  `id_jugador` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Partida_Offline_Jugador1` (`id_jugador` ASC) ,
  CONSTRAINT `fk_Partida_Offline_Jugador1`
    FOREIGN KEY (`id_jugador` )
    REFERENCES `SprintGame`.`Jugador` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SprintGame`.`Partida_Online`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `SprintGame`.`Partida_Online` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `id_jugador1` INT NOT NULL ,
  `id_jugador2` INT NOT NULL ,
  `distancia` DOUBLE NOT NULL ,
  `temps_jugador1` TIME NULL ,
  `temps_jugador2` TIME NULL ,
  `id_guanyador` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Partida_Online_Jugador1` (`id_jugador1` ASC) ,
  INDEX `fk_Partida_Online_Jugador2` (`id_jugador2` ASC) ,
  INDEX `fk_Partida_Online_Jugador3` (`id_guanyador` ASC) ,
  CONSTRAINT `fk_Partida_Online_Jugador1`
    FOREIGN KEY (`id_jugador1` )
    REFERENCES `SprintGame`.`Jugador` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Partida_Online_Jugador2`
    FOREIGN KEY (`id_jugador2` )
    REFERENCES `SprintGame`.`Jugador` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Partida_Online_Jugador3`
    FOREIGN KEY (`id_guanyador` )
    REFERENCES `SprintGame`.`Jugador` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
